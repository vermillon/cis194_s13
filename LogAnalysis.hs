{-# OPTIONS_GHC -Wall #-}
module LogAnalysis where
import Log

a2i :: String -> Int
a2i s = read s :: Int

parseMessage :: String -> LogMessage
parseMessage s = case words s of
    ("E" : prio : tim : msgs) -> LogMessage (Error $ a2i prio) (a2i tim) (unwords msgs)
    ("I" : tim : msgs)        -> LogMessage Info (a2i tim) (unwords msgs)
    ("W" : tim : msgs)        -> LogMessage Warning (a2i tim) (unwords msgs)
    _                         -> Unknown s

parse :: String -> [LogMessage]
parse = map parseMessage . lines

insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) tree = tree
insert lm Leaf = Node Leaf lm Leaf
insert lm@(LogMessage _ t _) mt
    | t < tRoot = Node (insert lm left) root right
    | otherwise = Node left root (insert lm right)
    where
        (Node left root@(LogMessage _ tRoot _) right) = mt

build :: [LogMessage] -> MessageTree
build = foldr insert Leaf

inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node left root right) = (inOrder left) ++ [root] ++ (inOrder right)

isRelevant :: LogMessage -> Bool
isRelevant (LogMessage (Error prio) _ _) = prio >= 50
isRelevant _ = False

getMessage :: LogMessage -> String
getMessage (LogMessage _ _ msg) = msg
getMessage _ = ""

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong = map getMessage . filter isRelevant . inOrder . build
