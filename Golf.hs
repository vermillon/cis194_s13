{-# OPTIONS_GHC -Wall #-}
module Golf where
import Data.List

skip :: [a] -> Int -> [a]
skip l n = case drop (n-1) l of
                [] -> []
                (x:xs) -> x : skip xs n

skips :: [a] -> [[a]]
skips l = map (skip l) [1..length l]


localMaxima :: [Integer] -> [Integer]
localMaxima xs = [x | (w:x:y:_) <- tails xs, x>w && x>y]


histoLine :: [Int] -> Int -> String
histoLine cnts k = map (\n -> if n >= k then '*' else ' ') cnts

histogram :: [Integer] -> String
histogram l = unlines (reverse $ map (histoLine cnts) [1..maximum cnts]) ++ "==========\n0123456789\n"
    where cnts = map (\n -> length $ filter (==n) l) [0..9]
