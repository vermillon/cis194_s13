{-# OPTIONS_GHC -Wall #-}
module HW4 where
import Data.List

fun1 :: [Integer] -> Integer
fun1 []     = 1
fun1 (x:xs)
 | even x    = (x - 2) * fun1 xs
 | otherwise = fun1 xs

fun1' :: [Integer] -> Integer
fun1' = foldl (\acc x -> (x-2) * acc ) 1 . (filter even)

fun2 :: Integer -> Integer
fun2 1 = 0
fun2 n 
 | even n    = n + fun2 (n `div` 2) --28 14 22 34 52 26 40 20 10 5 16 8 4 2
 | otherwise = fun2 (3 * n + 1)  --

fun2' :: Integer -> Integer
fun2' = sum . filter even . takeWhile (>1) . iterate (\m -> if even m then m `div` 2 else 3*m+1)

--

data Tree a = Leaf
            | Node Integer (Tree a) a (Tree a)
    deriving (Show, Eq)

getDepth :: Tree a -> Integer
getDepth Leaf = -1
getDepth (Node d _ _ _) = d

isBalanced :: Tree a -> Bool
isBalanced Leaf = True
isBalanced (Node _ left _ right) = (abs (getDepth left - getDepth right) <= 1) && isBalanced left && isBalanced right

addToTree :: a -> Tree a -> Tree a
addToTree e Leaf = Node 0 Leaf e Leaf
addToTree e (Node n left root right)
 | getDepth left > getDepth right = Node n left root (addToTree e right)
 | otherwise = Node (1 + getDepth newLeft) newLeft root right
    where newLeft = addToTree e left

foldTree :: [a] -> Tree a
foldTree = foldr addToTree Leaf

--

xor :: [Bool] -> Bool
xor = foldl (\x y -> if x then not y else y) False

--

map' :: (a -> b) -> [a] -> [b]
map' f = foldr (\x acc -> f x : acc) []

--

myFoldl :: (a -> b -> a) -> a -> [b] -> a
myFoldl f base xs = foldr (\x y -> y `f` x) base xs

--

sieveSundaram :: Integer -> [Integer]
sieveSundaram n = map ((+1).(*2)) $ [1..n] \\ [i+j+2*i*j | i <- [1..n], j <- [1..n], i<=j]
